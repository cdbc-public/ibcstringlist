# IStringlist

## Description
IStringList is a stringlist implemented as a COM-service, it directly maps
1:1 to TStringList from FPC's RTL. It supports enumerators and exposes its
underlying 'TStringlist' via the readonly 'List'-property, for easy
collaboration with other objects.

## Usage
```pascal
procedure TfrmIntfMain.btnUtf8Click(Sender: TObject);
var lst: IStringList;
begin
  if dlgOpen.Execute then begin
    lst:= CreStrListFromFile(dlgOpen.FileName);
    lst.Sort;
    { the old way }
  //  Listbox1.Items.Clear;
  //  Listbox1.Items.Assign(lst.List);
    { the new way ~ 26.11,24 }
    lst.AssignToEx(Listbox1.Items,true);
  end;
end;
```
For further examples of use see "MVP-Setup" project in my
Gitlab account, where it's used extensively.

## Author
Benny Christensen a.k.a. cdbc

## License
BSD 3 Clause license


## Project status
~~~ 
02.05.2024 Initial commit, version 3.02.05.2024
31.05.2024 Bugfix commit, version 4.31.05.2024
09.06.2024 Refactor commit, version 6.09.06.2024
31.07.2024 Updated readme, code version 6.09.06.2024
20.08.2024 Updated istrlist.pas, version 7.20.08.2024
           (2 new factories & 2 new 'ForEach')
26.11.2024 Updated istrlist.pas, version 8.26.11.2024
           (added: AssignToEx() & ScanFor() to IStrings)
28.11.2024 Updated istrlist.pas, version 9.28.11.2024
           (added feature: 'AddOrSet' to IStrings - thanks Thaddy)
~~~ 



